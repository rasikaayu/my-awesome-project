<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![MIT License][license-shield]][license-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="logo.png" alt="Logo">
  </a>

  <h3 align="center">Advocato Lawyer Workbench Prototype</h3>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Usage](#usage)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

[![Advocato][product-screenshot]](https://advocato.herokuapp.com)
> Advocato is a work platform for the community, especially lawyers, or legal aid institutions to share and store documents relating to the cases they are working on.
> <br> Providing access to legal assistance for disadvantaged communities, both by commercial lawyers and by legal aid agencies.
> <br> We provide a place where user can save their documents privately and can access it everytime they need, a place where people can share documents, hence they also can search for related documents for their cases.
> We provide corporates to share documents among the corporate's lawyers, and set collaborations for corporate's lawyers so they can connect with each other to finish the cases they are working on.
> <br>


To try the project, you can [click this link](https://advocato.herokuapp.com) or go to advocato.herokuapp.com


### Built With
This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Django](https://djangoproject.com)
* [SQLite](https://www.sqlite.org)
* [Docker](https://www.docker.com)



<!-- USAGE EXAMPLES -->
## Usage

Advocato is a lawyer's workbench where it can be accessed by some roles. There are four roles here. 
<br> You can log in as a retail user, a corporate user, or a corporate admin. And there's another role who can have access to manage everything, we called it superadmin. 
<br> A corporate admin's account is made by superadmin. A corporate user's account is made by corporate admin. 
<br> So if you dont belong to any corporate or just want to have an account as retail user, you can go create a new account by registering yourself on the website. 

**Retail User**
<br>
As a retail user, you can access explore sidebar to find files from Mahkamah Agung that have been uploaded to that directory,
<br> you can upload your file in your private docs (filetype: .pdf) and you can also create projects where you can link documents to the project.
![Advocato][product-retail]
<br>
Each documents on the "explore" sidebar have its details consist of the summary of the documents. 
<br> You can see the full text of the document if you click "download", from there you can also download it as pdf.
![Advocato][product-retail2]
**Corporate Admin**
<br>
As a corporate admin, you can manage users who work in the corporate (create, edit, or delete corporate users' account).
<br> There is a corporate repository which everyone in corporate could access. The people who can upload a document to that repository is corporate admin,
<br> so that the document they uploaded can be seen by all corporate's members. 
![Advocato][product-corpadmin]
<br>
**Superadmin**
<br>
Superadmin can manage all corporates who have been registered (create, edit, or delete corporates), if a corporate wants to use this workbench they have to contact us
<br> first then the superadmin will create a new corporate repository for them so they can use it. Superadmin can also manage all users' accounts registered in the system,
<br> including corporate admin's accounts. 
![Advocato][product-admin1]
There is a public repository where all users can access but only superadmin who has access to upload document to the repository. 
<br> The public documents that have been uploaded has it details if you click the filename on the list, there you can see the versions of the documents
<br> and everyone can leave comments there. The purpose of this public repository is for sharing. 
<br><br>
The version of a document will be updated if the superadmin upload the document with existing filename in the repository.
You can see and download all the version of the document on document details.
![Advocato][product-admin2]

<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/akhirnya-lawyer-benchmark-prototype.git](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/akhirnya-lawyer-benchmark-prototype.git)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Font Awesome](https://fontawesome.com)





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=flat-square
[contributors-url]: https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/akhirnya-lawyer-benchmark-prototype/-/graphs/staging
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[product-screenshot]: screenshot.png
[product-retail]: retailuser.png
[product-retail2]: exploresummary.png
[product-corpadmin]: corpadmin.png
[product-admin1]: superadmin.png
[product-admin2]: superadmin.png